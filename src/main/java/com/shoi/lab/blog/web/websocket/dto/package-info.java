/**
 * Data Access Objects used by WebSocket services.
 */
package com.shoi.lab.blog.web.websocket.dto;
