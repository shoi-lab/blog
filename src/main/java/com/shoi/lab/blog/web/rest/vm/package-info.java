/**
 * View Models used by Spring MVC REST controllers.
 */
package com.shoi.lab.blog.web.rest.vm;
